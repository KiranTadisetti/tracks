import React from 'react'
import { createAppContainer,createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import AccountScreen from './src/screens/AccountScreen'
import TrackDetailScreen from './src/screens/TrackDetailScreen'
import TrackCreateScreen from './src/screens/TrackCreateScreen'
import SinginScreen from './src/screens/SinginScreen'
import SignupScreen from './src/screens/SignupScreen'
import TrackListScreen from './src/screens/TrackListScreen';

import {Provider as AuthProvider} from './src/context/AuthContext'


const switchNavigator =createSwitchNavigator({
  loginFlow: createStackNavigator({
    Signup: SignupScreen,
    Signin:SinginScreen
  }),
  mainFlow:createBottomTabNavigator({
    trackListFlow:createStackNavigator({
      TrackList:TrackListScreen,
      TrackDetails:TrackDetailScreen
    }),
    TrackCreate:TrackCreateScreen,
    Account:AccountScreen
  })

});

const App= createAppContainer(switchNavigator);

export default ()=>{
  return (
    <AuthProvider>
      <App/>
    </AuthProvider>
  )
}